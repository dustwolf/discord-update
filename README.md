# Discord update

This is an Ansible script for automatically updating Discord in a better way on Ubuntu.

## Usage

Because the script is designed to be used headless with minimal configuration changes on the target system, it should be run as root (so it can install updates), but be provided with the username of the user that is going to be using it, because discord settings are specific to each user.

```bash
sudo ansible-playbook -i inventory playbook.yml -e discord_user=username
```

This will configure Discord and download an update immediately. To do this automatically see [Installation](#installation) below.

## Installation

To run ansible, you will need ansible: `apt install ansible`

For the sake of convenience, this script is provided with an installer that sets it up with systemd to update weekly. To run this installer, run:

```bash
sudo ansible-playbook -i inventory install.yml -e discord_user=username
```

See [Usage](#usage) section above for information regarding the `discord_user` parameter.

Do not delete the ansible files from this location after installation, as it configures to run the files from this location.

Be aware that this script will use about 100 MB of internet download bandwidth weekly.

### Modify or uninstall

To alter or remove the installation, modify or delete the files:
```
/etc/systemd/system/discord-update.service
/etc/systemd/system/discord-update.timer
```

Then run:
```bash
systemctl daemon-reload
```

If you modified the timer file, also run:
```bash
systemctl restart discord-update.timer
```

### Check update service status

Discord will not demand an update on startup when configured using this script. To see if your installation is updating, you may check:
```bash
journalctl -u discord-update
```

### Force update

To force an immediate update, run:
```bash
systemctl start discord-update
```

## License
[MIT](LICENSE)

## Roadmap

The code currently fully downloads an update whether a new version is provided or not. This is the best we can do, given that Ansible does not provide a convenient way to parse the redirect to resolve the latest version, and Discord does not provide checksums for deb files.

(Needless to say all of this would be much easier if Discord provided a repository like all other Linux software providers, but it seems clear that they will not.)

I've noticed however that Discord's file download infrastructure is Google's S3 storage and while their API is not generally open to public access, it seems concievable that getting into the internals of these protocols, it would be possible to glean whether the download file has changed, so that it does not have to be downloaded every time. I am open to suggestions, code contributions, etc, in this regard.

## Project status

This is a working solution. I am using this for my private use and I will contribute as time allows.

You are free to use this code if you find it useful.
